"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _reactHookForm = require("react-hook-form");

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var ReactHookFormInput = function ReactHookFormInput(_ref, ref) {
  var Component = _ref.Component,
      name = _ref.name,
      defaultValue = _ref.defaultValue,
      control = _ref.control,
      rules = _ref.rules,
      onChange_ = _ref.onChange,
      onBlur_ = _ref.onBlur,
      rest = _objectWithoutProperties(_ref, ["Component", "name", "defaultValue", "control", "rules", "onChange", "onBlur"]);

  var internalRef = (0, _react.useRef)();
  var setRef = (0, _react.useCallback)(function (instance) {
    internalRef.current = instance;

    if (ref) {
      if (typeof ref === 'function') {
        ref(instance);
      } else {
        ref.current = instance;
      }
    }
  }, []);
  var onFocus = (0, _react.useCallback)(function () {
    // internalRef.current.disabled = false
    internalRef.current.focus();
  }, []); // `feact-hook-form` doesn't know how to properly handle `undefined` values.
  // https://github.com/react-hook-form/react-hook-form/issues/2990

  defaultValue = defaultValue === undefined ? null : defaultValue;
  return _react["default"].createElement(_reactHookForm.Controller, {
    control: control,
    name: name,
    defaultValue: defaultValue,
    rules: rules,
    onFocus: onFocus,
    render: function render(_ref2) {
      var value = _ref2.value,
          onChange = _ref2.onChange,
          onBlur = _ref2.onBlur;
      var onChangeCombined = (0, _react.useCallback)(function (value) {
        onChange(value);

        if (onChange_) {
          onChange_(value);
        }
      }, [onChange, onChange_]);
      var onBlurCombined = (0, _react.useCallback)(function (event) {
        onBlur(event);

        if (onBlur_) {
          onBlur_(event);
        }
      }, [onBlur, onBlur_]);
      return _react["default"].createElement(Component, _extends({
        ref: setRef
      }, rest, {
        value: value,
        onChange: onChangeCombined,
        onBlur: onBlurCombined
      }));
    }
  });
};

ReactHookFormInput = _react["default"].forwardRef(ReactHookFormInput);
ReactHookFormInput.propTypes = {
  Component: _propTypes["default"].elementType.isRequired,
  name: _propTypes["default"].string.isRequired,
  defaultValue: _propTypes["default"].string,
  control: _propTypes["default"].object.isRequired,
  rules: _propTypes["default"].object.isRequired,
  onChange: _propTypes["default"].func,
  onBlur: _propTypes["default"].func
};
var _default = ReactHookFormInput;
exports["default"] = _default;
//# sourceMappingURL=ReactHookFormInput.js.map