function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

import React, { useRef, useCallback } from 'react';
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types';

var ReactHookFormInput = function ReactHookFormInput(_ref, ref) {
  var Component = _ref.Component,
      name = _ref.name,
      defaultValue = _ref.defaultValue,
      control = _ref.control,
      rules = _ref.rules,
      onChange_ = _ref.onChange,
      onBlur_ = _ref.onBlur,
      rest = _objectWithoutProperties(_ref, ["Component", "name", "defaultValue", "control", "rules", "onChange", "onBlur"]);

  var internalRef = useRef();
  var setRef = useCallback(function (instance) {
    internalRef.current = instance;

    if (ref) {
      if (typeof ref === 'function') {
        ref(instance);
      } else {
        ref.current = instance;
      }
    }
  }, []);
  var onFocus = useCallback(function () {
    // internalRef.current.disabled = false
    internalRef.current.focus();
  }, []); // `feact-hook-form` doesn't know how to properly handle `undefined` values.
  // https://github.com/react-hook-form/react-hook-form/issues/2990

  defaultValue = defaultValue === undefined ? null : defaultValue;
  return React.createElement(Controller, {
    control: control,
    name: name,
    defaultValue: defaultValue,
    rules: rules,
    onFocus: onFocus,
    render: function render(_ref2) {
      var value = _ref2.value,
          onChange = _ref2.onChange,
          onBlur = _ref2.onBlur;
      var onChangeCombined = useCallback(function (value) {
        onChange(value);

        if (onChange_) {
          onChange_(value);
        }
      }, [onChange, onChange_]);
      var onBlurCombined = useCallback(function (event) {
        onBlur(event);

        if (onBlur_) {
          onBlur_(event);
        }
      }, [onBlur, onBlur_]);
      return React.createElement(Component, _extends({
        ref: setRef
      }, rest, {
        value: value,
        onChange: onChangeCombined,
        onBlur: onBlurCombined
      }));
    }
  });
};

ReactHookFormInput = React.forwardRef(ReactHookFormInput);
ReactHookFormInput.propTypes = {
  Component: PropTypes.elementType.isRequired,
  name: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  control: PropTypes.object.isRequired,
  rules: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  onBlur: PropTypes.func
};
export default ReactHookFormInput;
//# sourceMappingURL=ReactHookFormInput.js.map