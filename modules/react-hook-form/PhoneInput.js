function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import ReactHookFormInput from './ReactHookFormInput';
import PhoneInput_ from '../PhoneInput';
import { metadata as metadataType } from '../PropTypes';
export function createPhoneInput(defaultMetadata) {
  var PhoneInput = function PhoneInput(props, ref) {
    return React.createElement(ReactHookFormInput, _extends({}, props, {
      ref: ref,
      Component: PhoneInput_
    }));
  };

  PhoneInput = React.forwardRef(PhoneInput);
  PhoneInput.propTypes = {
    metadata: metadataType.isRequired
  };
  PhoneInput.defaultProps = {
    metadata: defaultMetadata
  };
  return PhoneInput;
}
export default createPhoneInput();
//# sourceMappingURL=PhoneInput.js.map