function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import PhoneTextInput from './PhoneTextInput';
import PhoneInput_ from '../PhoneInput';
import { metadata as metadataType } from '../PropTypes';
/**
 * This is an _experimental_ React Native component.
 * Feedback thread: https://github.com/catamphetamine/react-phone-number-input/issues/296
 */

export function createPhoneInput(defaultMetadata) {
  var PhoneInput = function PhoneInput(props, ref) {
    return React.createElement(PhoneInput_, _extends({}, props, {
      ref: ref,
      smartCaret: false,
      inputComponent: PhoneTextInput
    }));
  };

  PhoneInput = React.forwardRef(PhoneInput);
  PhoneInput.propTypes = {
    metadata: metadataType.isRequired
  };
  PhoneInput.defaultProps = {
    metadata: defaultMetadata
  };
  return PhoneInput;
}
export default createPhoneInput();
//# sourceMappingURL=PhoneInput.js.map